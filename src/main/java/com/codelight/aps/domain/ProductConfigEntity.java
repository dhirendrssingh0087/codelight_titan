package com.codelight.aps.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "product_config")
public class ProductConfigEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "config_id")
	private Integer configId;
	
	@Column(name = "product")
	private String product;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "value")
	private String value;

}
