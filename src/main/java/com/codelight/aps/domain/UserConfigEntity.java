package com.codelight.aps.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "user_config")
public class UserConfigEntity  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "config_id")
	private Integer configId;
	
	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "app_code")
	private String appCode;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "value")
	private String value;

	
	@Column(name = "latest_version")
	private boolean latestVersion;
	
	@Column(name = "change_log")
	private String changeLog;

	public Integer getConfigId() {
		return configId;
	}

	public void setConfigId(Integer configId) {
		this.configId = configId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isLatestVersion() {
		return latestVersion;
	}

	public void setLatestVersion(boolean latestVersion) {
		this.latestVersion = latestVersion;
	}

	public String getChangeLog() {
		return changeLog;
	}

	public void setChangeLog(String changeLog) {
		this.changeLog = changeLog;
	}
	
	

}
