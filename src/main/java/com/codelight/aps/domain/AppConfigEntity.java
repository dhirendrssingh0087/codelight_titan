package com.codelight.aps.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "app_config")
public class AppConfigEntity  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "config_id")
	private Integer configId;
	
	@Column(name = "app_code")
	private String appCode;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "value")
	private String value;

	
	@Column(name = "is_lookup_value")
	private boolean isLookupValue;
	
	@Column(name = "lookup_source")
	private String lookupSource;

}
