package com.codelight.aps.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.codelight.aps.domain.AppConfigEntity;
import com.codelight.aps.domain.ProductConfigEntity;
import com.codelight.aps.domain.RegionalConfigEntity;
import com.codelight.aps.domain.UserConfigEntity;
import com.codelight.aps.dto.UserConfigRequest;
import com.codelight.aps.repository.AppConfigRepository;
import com.codelight.aps.repository.ProductConfigRepository;
import com.codelight.aps.repository.RegionalConfigRepository;
import com.codelight.aps.repository.UserConfigRepository;
import com.codelight.aps.service.IAppConfigService;

@Service
public class AppConfigService implements IAppConfigService {
	
	@Autowired
	private AppConfigRepository appConfigRepository;

	@Autowired
	private UserConfigRepository userConfigRepository;
	
	@Autowired
	private RegionalConfigRepository regionalConfigRepository;
	
	@Autowired
	private ProductConfigRepository productConfigRepository;
	

	@Override
	public Map<String, Object> fetchAppConfig(String user, String appCode) {
		List<AppConfigEntity> appConfigs = appConfigRepository.findAllByAppCode(appCode);
		Map<String, Object> config = new HashMap<>();
		appConfigs.forEach(c->{
			config.put(c.getName(), c.getValue());
		});
		List<UserConfigEntity> userConfigs = userConfigRepository.findAllByUserId(user);
		Map<String,String> userMap = new HashMap<>();
		userConfigs.forEach(u->{
			userMap.put(u.getName(), u.getValue());
		});
		if(!CollectionUtils.isEmpty(userConfigs)) {
			userConfigs.forEach(c->{
					config.put(c.getName(), c.getValue());
			});
		}
		
		List<AppConfigEntity>  lookupConfigs = appConfigs.stream()
										.filter(c->c.isLookupValue())
										.collect(Collectors.toList());
		
		if(!CollectionUtils.isEmpty(lookupConfigs)) {
			lookupConfigs.stream().filter(c->c.getLookupSource().equals("Regional Config")).forEach(c->{
				List<RegionalConfigEntity> regionConfigs = regionalConfigRepository.findAllByRegion(userMap.get(c.getName()));
				if(!CollectionUtils.isEmpty(regionConfigs)) {
					regionConfigs.forEach(r->{
						config.put(r.getName(), r.getValue());
					});
				}
				
			});
		}
		
		if(!CollectionUtils.isEmpty(lookupConfigs)) {
			lookupConfigs.stream().filter(c->c.getLookupSource().equals("Product Config")).forEach(c->{
				List<ProductConfigEntity> productConfigs = productConfigRepository.findAllByProduct(userMap.get(c.getName()));
				if(!CollectionUtils.isEmpty(productConfigs)) {
					productConfigs.forEach(p->{
						config.put(p.getName(), p.getValue());
					});
				}
				
			});
		}
		
		return config;
	}


	@Override
	public void saveConfig(UserConfigRequest request) {
	}

	

}
