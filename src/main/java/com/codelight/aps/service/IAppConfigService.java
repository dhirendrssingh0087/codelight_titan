package com.codelight.aps.service;

import java.util.Map;

import com.codelight.aps.dto.UserConfigRequest;

public interface IAppConfigService {

	Map<String, Object> fetchAppConfig(String user, String appCode);
	
	void saveConfig(UserConfigRequest request);

}
