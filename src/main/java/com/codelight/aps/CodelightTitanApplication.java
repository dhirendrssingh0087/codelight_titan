package com.codelight.aps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodelightTitanApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodelightTitanApplication.class, args);
	}

}
