package com.codelight.aps.api;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codelight.aps.dto.UserConfigRequest;
import com.codelight.aps.service.impl.AppConfigService;

@RestController
@RequestMapping("/appconfig")
public class AppConfigResource {
	
	private static Logger log = LoggerFactory.getLogger(AppConfigResource.class);
	
	@Autowired
	AppConfigService configService;
	
	@Autowired
	@GetMapping("/fetchAll")
	@ResponseBody
	public Map<String, Object> fetchAppConfig() {
		return configService.fetchAppConfig("prshukla", "code_light_app");
	}
	
	@PostMapping("/save")
	public String saveConfig(UserConfigRequest request) {
		log.info("Received Request : {}", request);
		return "saved";
	}

}
