package com.codelight.aps.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.codelight.aps.domain.AppConfigEntity;

@Repository
public interface AppConfigRepository extends JpaRepository<AppConfigEntity, Integer> {

	List<AppConfigEntity> findAllByAppCode(String appCode);

}
