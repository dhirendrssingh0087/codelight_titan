package com.codelight.aps.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.codelight.aps.domain.ProductConfigEntity;
import com.codelight.aps.domain.RegionalConfigEntity;

@Repository
public class ProductConfigRepository {

	@PersistenceContext
	 private EntityManager entityManager;
	 
	 
	public List<ProductConfigEntity> findAllByProduct(String product) {
		return  entityManager.createQuery("FROM ProductConfigEntity p WHERE p.product = :product")
              .setParameter("product", product)
              .getResultList();
	}

}
