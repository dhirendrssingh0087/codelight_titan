package com.codelight.aps.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.codelight.aps.domain.RegionalConfigEntity;

@Repository
public class RegionalConfigRepository  {

	@PersistenceContext
	 private EntityManager entityManager;
	 
	 
	public List<RegionalConfigEntity> findAllByRegion(String region) {
		return  entityManager.createQuery("FROM RegionalConfigEntity p WHERE p.region = :region")
               .setParameter("region", region)
               .getResultList();
	}
	

}
