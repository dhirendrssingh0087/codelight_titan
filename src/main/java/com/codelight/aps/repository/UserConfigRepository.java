package com.codelight.aps.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.codelight.aps.domain.UserConfigEntity;

@Service
public class UserConfigRepository{

	
	@PersistenceContext
	private EntityManager entityManager;
	 
	 
	public List<UserConfigEntity> findAllByUserId(String user) {
		return  entityManager.createQuery("FROM UserConfigEntity u WHERE u.userId = :user")
                .setParameter("user", user)
                .getResultList();
	}
	
}

	
