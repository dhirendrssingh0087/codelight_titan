package com.codelight.aps.dto;

import lombok.Data;

@Data
public class UserConfigRequest {

	private String name;
	private String user;
	private String value;
	private String appcode;
}
