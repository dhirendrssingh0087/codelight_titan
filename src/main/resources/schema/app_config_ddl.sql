CREATE TABLE `aps`.`app_config` (
  `config_id` INT NOT NULL,
  `app_code` VARCHAR(50) NOT NULL,
  `name` VARCHAR(1000) NULL,
  `value` JSON NULL,
  `is_lookup_value` BIT NULL,
  `lookup_source` VARCHAR(50) NULL,
  PRIMARY KEY (`app_config_id`));

  CREATE TABLE `aps`.`user_config` (
  `config_id` INT NOT NULL,
  `app_code` VARCHAR(50) NOT NULL,
  `user_id` INT NOT NULL,
  `name` VARCHAR(1000) NOT NULL,
  `value` JSON NOT NULL,
  `latest_version` INT,
  `change_log` JSON NULL,
  PRIMARY KEY (`user_config_id`));
  
  
  CREATE TABLE `regional_config` (
  `config_id` int NOT NULL,
  `name` varchar(1000) NOT NULL,
  `value` json NOT NULL,
  `region` varchar(100) NOT NULL,
  PRIMARY KEY (`regional_config_id`));
  
  CREATE TABLE `aps`.`product_config` (
  `config_id` int NOT NULL,
  `name` varchar(1000) NOT NULL,
  `value` json NOT NULL,
  `product` varchar(100) NOT NULL,
  PRIMARY KEY (`product_config_id`));
  
  ALTER TABLE `aps`.`app_config` 
CHANGE COLUMN `value` `value` VARCHAR(4000) NULL DEFAULT NULL ;

  ALTER TABLE `aps`.`user_config` 
CHANGE COLUMN `value` `value` VARCHAR(4000) NULL DEFAULT NULL ;

  ALTER TABLE `aps`.`regional_config` 
CHANGE COLUMN `value` `value` VARCHAR(4000) NULL DEFAULT NULL ;
  
  
 ALTER TABLE `aps`.`product_config` 
CHANGE COLUMN `value` `value` VARCHAR(4000) NULL DEFAULT NULL ;

ALTER TABLE `aps`.`user_config` 
CHANGE COLUMN `user_id` `user_id` VARCHAR(50) NOT NULL ;