CREATE TABLE `aps`.`column_definition` (
  `column_id` INT NOT NULL,
  `name` VARCHAR(1000) NOT NULL,
  `header_text` VARCHAR(100) NULL,
  `data_type` VARCHAR(20) NULL,
  `col_def_json` VARCHAR(4000) NULL,
  `create_dt` DATETIME NULL,
  PRIMARY KEY (`column_id`));
  
 
 CREATE TABLE `aps`.`app_functions` (
  `function_id` INT NOT NULL,
  `function_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`function_id`));
  
  CREATE TABLE `aps`.`function_column_mapping` (
  `function_id` INT NOT NULL,
  `column_id` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`function_id`, `column_id`));
  
  CREATE TABLE `aps`.`function_template` (
  `function_id` int NOT NULL,
  `user_id` int NOT NULL,
  `system_template` bit(1) DEFAULT NULL,
  `template_id` varchar(50) NOT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `is_user_default` bit(1) DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`function_id`,`template_id`);
  
  CREATE TABLE `aps`.`function_template_details` (
  `template_id` BIGINT NOT NULL,
  `detail_id` BIGINT NOT NULL,
  `detail_name` VARCHAR(50) NULL,
  `detail_param` VARCHAR(2000) NULL,
  `create_dt` DATETIME NULL,
  PRIMARY KEY (`template_id`, `detail_id`));
  
  
  CREATE TABLE `aps`.`app_views` (
  `view_id` INT NOT NULL,
  `name` VARCHAR(1000) NULL,
  `function_id` INT NULL,
  `description` VARCHAR(50) NULL,
  `created_dt` DATETIME NULL,
  PRIMARY KEY (`view_id`));
  
  CREATE TABLE `aps`.`view_grid_state` (
  `grid_state_id` INT NOT NULL,
  `view_id` INT NULL,
  `column_state` VARCHAR(2000) NULL,
  `filter_model` VARCHAR(2000) NULL,
  `quick_filter` VARCHAR(2000) NULL,
  `sort_model` VARCHAR(2000) NULL,
  PRIMARY KEY (`grid_state_id`));
  
  
  CREATE TABLE `aps`.`app_config_default` (
  `config_id` INT NOT NULL,
  `config_data` VARCHAR(1000) NULL,
  `function_id` INT NULL,
  `change_log` VARCHAR(100) NULL,
  PRIMARY KEY (`config_id`));
  
  
  CREATE TABLE `aps`.`app_config_region` (
  `config_id` INT NOT NULL,
  `config_region_id` VARCHAR(25) NOT NULL,
  `config_data` VARCHAR(1000) NULL,
  `function_id` INT NULL,
  `change_log` VARCHAR(100) NULL,
  PRIMARY KEY (`config_id`));
  
  CREATE TABLE `aps`.`app_config_user` (
  `config_id` INT NOT NULL,
  `config_region_id` VARCHAR(25) NOT NULL,
  `user_id` VARCHAR(25) NOT NULL,
  `config_data` VARCHAR(1000) NULL,
  `function_id` INT NULL,
  `change_log` VARCHAR(100) NULL,
  PRIMARY KEY (`config_id`));
  
  CREATE TABLE `aps`.`user_info` (
  `user_id` INT NOT NULL,
  `user_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`user_id`));
  
  
  
  