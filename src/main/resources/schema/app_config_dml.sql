INSERT INTO `aps`.`app_config`(`config_id`,`app_code`,`name`,`value`,`is_lookup_value`,`lookup_source`)
VALUES(1, 'code_light_app', 'colDefinitions','[
            {
                "field": "isin",
                "headerName": "ISIN",
                "filter": true,
                "width": 144,
                "hide": false,
                "resizable": true,
                "sortable": true
            },
            {
                "field": "cusip",
                "headerName": "CUSIP",
                "filter": true,
                "width": 112,
                "hide": false,
                "resizable": true,
                "sortable": true
            },
            {
                "field": "currency",
                "headerName": "CCY",
                "filter": true,
                "width": 92,
                "hide": false,
                "resizable": true,
                "sortable": true
            },
            {
                "field": "securityId",
                "headerName": "Security ID",
                "filter": true,
                "width": 100,
                "hide": true,
                "resizable": true,
                "sortable": true
            },
            {
                "field": "ticker",
                "headerName": "Ticker",
                "filter": true,
                "width": 160,
                "hide": false,
                "resizable": true,
                "sortable": true
            },
            {
                "field": "quoteType",
                "headerName": "Quote",
                "filter": true,
                "width": 120,
                "hide": false,
                "resizable": true,
                "sortable": true
            },
            {
                "field": "position",
                "headerName": "Position",
                "filter": true,
                "width": 114,
                "hide": false,
                "resizable": true,
                "sortable": true
            },
            {
                "field": "snpRating",
                "headerName": "Rating",
                "filter": true,
                "width": 125,
                "hide": false,
                "resizable": true,
                "sortable": true
            },
            {
                "field": "bidPrice",
                "headerName": "Bid Px",
                "filter": true,
                "width": 114,
                "hide": false,
                "resizable": true,
                "sortable": true,
                "enableCellChangeFlash": true
            },
            {
                "field": "askPrice",
                "headerName": "Ask Px",
                "filter": true,
                "width": 114,
                "hide": false,
                "resizable": true,
                "sortable": true,
                "enableCellChangeFlash": true
            },
            {
                "field": "bidYield",
                "headerName": "Bid Yld",
                "filter": true,
                "width": 114,
                "hide": false,
                "resizable": true,
                "sortable": true,
                "enableCellChangeFlash": true
            },
            {
                "field": "askYield",
                "headerName": "Ask Yld",
                "filter": true,
                "width": 114,
                "hide": false,
                "resizable": true,
                "sortable": true,
                "enableCellChangeFlash": true
            },
            {
                "field": "bidSpread",
                "headerName": "Bid Sprd",
                "filter": true,
                "width": 122,
                "hide": false,
                "resizable": true,
                "sortable": true,
                "enableCellChangeFlash": true
            },
            {
                "field": "askSpread",
                "headerName": "Ask Sprd",
                "filter": true,
                "width": 122,
                "hide": false,
                "resizable": true,
                "sortable": true,
                "enableCellChangeFlash": true
            },
            {
                "field": "issuer",
                "headerName": "Issuer",
                "filter": true,
                "width": 70,
                "hide": true,
                "resizable": true,
                "sortable": true
            },
            {
                "field": "maturityDate",
                "headerName": "Mat. Date",
                "filter": true,
                "width": 70,
                "hide": true,
                "resizable": true,
                "sortable": true
            }
        ]',false,null);

INSERT INTO `aps`.`app_config`(`config_id`,`app_code`,`name`,`value`,`is_lookup_value`,`lookup_source`)VALUES(2, 'code_light_app', 'BuySellColor',true, false, null);
INSERT INTO `aps`.`app_config`(`config_id`,`app_code`,`name`,`value`,`is_lookup_value`,`lookup_source`)VALUES(3, 'code_light_app', 'Region','North Americat', true, 'Regional Config');
INSERT INTO `aps`.`app_config`(`config_id`,`app_code`,`name`,`value`,`is_lookup_value`,`lookup_source`)VALUES(4, 'code_light_app', 'Product','USCredit', true, 'Product Config');
INSERT INTO `aps`.`app_config`(`config_id`,`app_code`,`name`,`value`,`is_lookup_value`,`lookup_source`)VALUES(5, 'code_light_app', 'HttpTimeOut',5000, false, null);
INSERT INTO `aps`.`app_config`(`config_id`,`app_code`,`name`,`value`,`is_lookup_value`,`lookup_source`)VALUES(6, 'code_light_app', 'positionFormat','M', false, null);
INSERT INTO `aps`.`app_config`(`config_id`,`app_code`,`name`,`value`,`is_lookup_value`,`lookup_source`)VALUES(7, 'code_light_app', 'priceFormat',3, false, null);
INSERT INTO `aps`.`app_config`(`config_id`,`app_code`,`name`,`value`,`is_lookup_value`,`lookup_source`)VALUES(8, 'code_light_app', 'yieldFormat',4, false, null);
INSERT INTO `aps`.`app_config`(`config_id`,`app_code`,`name`,`value`,`is_lookup_value`,`lookup_source`)VALUES(9, 'code_light_app', 'spreadFromat',4, false, null);

INSERT INTO `aps`.`user_config`(`config_id`,`app_code`,`user_id`,`name`,`value`,`latest_version`,`change_log`)VALUES
(1,'code_light_app','prshukla','Region','Europe',1,null);
INSERT INTO `aps`.`user_config`(`config_id`,`app_code`,`user_id`,`name`,`value`,`latest_version`,`change_log`)VALUES
(2,'code_light_app','prshukla','Product','EURCredit',1,null);
INSERT INTO `aps`.`user_config`(`config_id`,`app_code`,`user_id`,`name`,`value`,`latest_version`,`change_log`)VALUES
(3,'code_light_app','dhsingh','Region','North America',1,null);
INSERT INTO `aps`.`user_config`(`config_id`,`app_code`,`user_id`,`name`,`value`,`latest_version`,`change_log`)VALUES
(4,'code_light_app','dhsingh','Product','USCredit',1,null);


INSERT INTO `aps`.`regional_config`(`config_id`,`name`,`value`,`region`)VALUES(1,'BuySellColor','Green','North America');
INSERT INTO `aps`.`regional_config`(`config_id`,`name`,`value`,`region`)VALUES(2,'BuySellColor','Red','Europe');

INSERT INTO `aps`.`product_config`(`config_id`,`name`,`value`,`product`)VALUES(1,'HttpTimeOut',3000,'USCredit');
INSERT INTO `aps`.`product_config`(`config_id`,`name`,`value`,`product`)VALUES(2,'HttpTimeOut',6000,'EURCredit');
