export const configData = [{ configId: 1, configSource: 'ROOT', configKey: 'priceFormat', configValue: 3, configChangeLog: [] },
{
    configId: 11, configSource: 'ROOT', configKey: 'colDefinitions', configValue: [
        {
            field: "isin",
            headerName: "ISIN",
            filter: true,
            width: 144,
            hide: false,
            resizable: true,
            sortable: true
        },
        {
            field: "cusip",
            headerName: "CUSIP",
            filter: true,
            width: 112,
            hide: false,
            resizable: true,
            sortable: true
        },
        {
            field: "currency",
            headerName: "CCY",
            filter: true,
            width: 92,
            hide: false,
            resizable: true,
            sortable: true
        },
        {
            field: "securityId",
            headerName: "Security ID",
            filter: true,
            width: 100,
            hide: true,
            resizable: true,
            sortable: true
        },
        {
            field: "ticker",
            headerName: "Ticker",
            filter: true,
            width: 160,
            hide: false,
            resizable: true,
            sortable: true
        },
        {
            field: "quoteType",
            headerName: "Quote",
            filter: true,
            width: 120,
            hide: false,
            resizable: true,
            sortable: true
        },
        {
            field: "position",
            headerName: "Position",
            filter: true,
            width: 114,
            hide: false,
            resizable: true,
            sortable: true
        },
        {
            field: "snpRating",
            headerName: "Rating",
            filter: true,
            width: 125,
            hide: false,
            resizable: true,
            sortable: true
        },
        {
            field: "bidPrice",
            headerName: "Bid Px",
            filter: true,
            width: 114,
            hide: false,
            resizable: true,
            sortable: true,
            enableCellChangeFlash: true
        },
        {
            field: "askPrice",
            headerName: "Ask Px",
            filter: true,
            width: 114,
            hide: false,
            resizable: true,
            sortable: true,
            enableCellChangeFlash: true
        },
        {
            field: "bidYield",
            headerName: "Bid Yld",
            filter: true,
            width: 114,
            hide: false,
            resizable: true,
            sortable: true,
            enableCellChangeFlash: true
        },
        {
            field: "askYield",
            headerName: "Ask Yld",
            filter: true,
            width: 114,
            hide: false,
            resizable: true,
            sortable: true,
            enableCellChangeFlash: true
        },
        {
            field: "bidSpread",
            headerName: "Bid Sprd",
            filter: true,
            width: 122,
            hide: false,
            resizable: true,
            sortable: true,
            enableCellChangeFlash: true
        },
        {
            field: "askSpread",
            headerName: "Ask Sprd",
            filter: true,
            width: 122,
            hide: false,
            resizable: true,
            sortable: true,
            enableCellChangeFlash: true
        },
        {
            field: "issuer",
            headerName: "Issuer",
            filter: true,
            width: 70,
            hide: true,
            resizable: true,
            sortable: true
        },
        {
            field: "maturityDate",
            headerName: "Mat. Date",
            filter: true,
            width: 70,
            hide: true,
            resizable: true,
            sortable: true
        }
    ], configChangeLog: []
},
{ configId: 2, configSource: 'ROOT', configKey: 'yieldFormat', configValue: 4, configChangeLog: [{ version: 1, value: 5 }, { version: 1, value: 7 }, { version: 1, value: 12 }] },
{ configId: 3, configSource: 'ROOT', configKey: 'spreadFromat', configValue: 2, configChangeLog: [] },
{ configId: 4, configSource: 'ROOT', configKey: 'invertBuySellColor', configValue: true, configChangeLog: [] },
{ configId: 5, configSource: 'ROOT', configKey: 'positionFormat', configValue: 'M', configChangeLog: [] },
{ configId: 6, configSource: 'ROOT', configKey: 'productGroup', configValue: 'USCredit', configChangeLog: [] },
{ configId: 7, configSource: 'USCredit', configKey: 'invertBuySellColor', configValue: false, configChangeLog: [] },
{ configId: 8, configSource: 'ROOT', configKey: 'Region', configValue: 'North America', configChangeLog: [] },
{ configId: 9, configSource: 'North America', configKey: 'priceFormat', configValue: 6, configChangeLog: [] },
{ configId: 10, configSource: 'User', configKey: 'priceFormat', configValue: 5, configChangeLog: [] },
]